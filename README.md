# Fancy Battleaxes

This is the official Git repository of Fancy Battleaxes, a Minecraft mod that adds Battleaxes, weapons with very high damage, but also very low attack Speed.

All Stats can be configured in the config file. Also certain Materials can be disabled in the config.
