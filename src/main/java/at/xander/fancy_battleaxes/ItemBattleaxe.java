package at.xander.fancy_battleaxes;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import at.xander.fancy_battleaxes.material.BattleaxeMaterial;
import at.xander.fancy_battleaxes.material.BattleaxeTier;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.block.state.BlockState;

public class ItemBattleaxe extends AxeItem {
	private static final float SharpnessImpact = 0.5f;
	private final BattleaxeTier tier;
	private ItemStack swordStack = null;

	private ItemStack getSwordStack() {
		if (swordStack == null) {
			swordStack = new ItemStack(Items.IRON_SWORD);
		}
		return swordStack;
	}

	public ItemBattleaxe(BattleaxeTier itemTier, Item.Properties properties) {
		super(itemTier, 1.0f, 1.0f, properties);
		this.tier = itemTier;
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment enchantment) {
		boolean appliesToSword = enchantment.canEnchant(getSwordStack());
		// Exclude sweeping enchantment.
		// Note: This is a hacky workaround to check if the enchantment is sweeping,
		// since there is no good way in current Minecraft versions, that I know
		return appliesToSword && enchantment != Enchantments.SWEEPING_EDGE;
	}

	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state) {
		int sharpness = stack.getEnchantmentLevel(Enchantments.SHARPNESS);
		float speed = super.getDestroySpeed(stack, state);
		if (speed == super.speed) {
			// The tool applies, but use the speed of the tier
			speed = tier.getSpeed();
		}
		// Add Sharpness to mining speed like Efficiency would
		return speed + (sharpness * sharpness + 1) * SharpnessImpact;
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot equipmentSlot, ItemStack stack) {
		// We have to create a new modMap
		Builder<Attribute, AttributeModifier> modMap = ImmutableMultimap.builder();
		if (equipmentSlot == EquipmentSlot.MAINHAND) {
			modMap.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon Modifier",
					(double) tier.getAttackDamage(), AttributeModifier.Operation.ADDITION));
			modMap.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon Modifier",
					(double) tier.getAttackSpeed(), AttributeModifier.Operation.ADDITION));
		}
		return modMap.build();
	}

	public BattleaxeMaterial getMaterial() {
		return tier.getMaterial();
	}

	@Override
	public int getMaxDamage(ItemStack stack) {
		return tier.getUses();
	}

	@Override
	public int getBarWidth(ItemStack stack) {
		return Math.round(13.0F - (float) stack.getDamageValue() * 13.0F / (float) stack.getMaxDamage());
	}

	@Override
	public int getBarColor(ItemStack stack) {
		float f = Math.max(0.0F,
				((float) (stack.getMaxDamage() - stack.getDamageValue())) / (float) stack.getMaxDamage());
		return Mth.hsvToRgb(f / 3.0F, 1.0F, 1.0F);
	}
}