package at.xander.fancy_battleaxes.crafting;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import at.xander.fancy_battleaxes.BattleaxeMod;
import at.xander.fancy_battleaxes.material.BattleaxeMaterial;
import net.minecraftforge.common.crafting.conditions.ICondition;

public record MaterialEnabledCondition(BattleaxeMaterial material) implements ICondition {
	public static final Codec<MaterialEnabledCondition> CODEC = RecordCodecBuilder.create(
			in -> in.group(Codec.STRING.fieldOf("material").xmap(s -> BattleaxeMaterial.valueOf(s.toUpperCase()), BattleaxeMaterial::name)
					.forGetter(MaterialEnabledCondition::material)).apply(in, MaterialEnabledCondition::new));

	@Override
	public boolean test(IContext context) {
		return BattleaxeMod.instance.config.toolEnables.get(material);
	}

	@Override
	public Codec<? extends ICondition> codec() {
		return CODEC;
	}

	@Override
	public String toString() {
		return "material_enabled(%s)".formatted(material);
	}

}
