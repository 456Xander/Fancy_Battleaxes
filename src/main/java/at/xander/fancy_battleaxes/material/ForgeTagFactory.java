package at.xander.fancy_battleaxes.material;

import java.util.function.Supplier;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;


public class ForgeTagFactory {
	public static TagKey<Item> createTag(String ore_name) {
		return ItemTags.create(new ResourceLocation("forge", ore_name));
	}

	public static Supplier<Ingredient> ingredientSupplier(String oreName) {
		TagKey<Item> tag = createTag(oreName);
		return () -> Ingredient.of(tag);
	}
}
