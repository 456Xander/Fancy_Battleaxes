package at.xander.fancy_battleaxes.material;

import net.minecraft.world.item.Tier;

public interface IBattleaxeTier extends Tier {
	float getAttackDamage();
	float getAttackSpeed();
}
