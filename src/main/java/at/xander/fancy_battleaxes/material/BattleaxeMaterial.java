package at.xander.fancy_battleaxes.material;

import java.util.function.Supplier;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Blocks;


public enum BattleaxeMaterial {
	STONE(1, 131, 4.0f, 7f, -3f, 5, () -> Ingredient.of(Blocks.COBBLESTONE), true),
	IRON(2, 250, 6.0f, 8f, -2.9f, 14, ForgeTagFactory.ingredientSupplier("ingots/iron"), true),
	DIAMOND(3, 1561, 8.0f, 10f, -2.8f, 10, ForgeTagFactory.ingredientSupplier("gems/diamond"), true),
	NETHERITE(4, 2031, 9.0f, 12f, -2.8f, 15, ForgeTagFactory.ingredientSupplier("ingots/netherite"), true),
	GOLD(0, 32, 12.0f, 8.0f, -3.1f, 21, ForgeTagFactory.ingredientSupplier("ingots/gold"), true),
	BRONZE(2, 360, 6.0f, 8f, -2.9f, 18, ForgeTagFactory.ingredientSupplier("ingots/bronze"), false),
	STEEL(2, 500, 7.0f, 9f, -2.9f, 10, ForgeTagFactory.ingredientSupplier("ingots/steel"), false),
	NICKEL(2, 300, 6.0f, 8f, -3f, 12, ForgeTagFactory.ingredientSupplier("ingots/nickel"), false),
	SILVER(2, 190, 9.0f, 8f, -2.8f, 20, ForgeTagFactory.ingredientSupplier("ingots/silver"), false),
	TITANIUM(3, 1000, 8.0f, 9f, -2.8f, 14, ForgeTagFactory.ingredientSupplier("ingots/titanium"), false),
	RUBY(3, 1024, 7.5f, 10f, -2.95f, 15, ForgeTagFactory.ingredientSupplier("gems/ruby"), false),
	SAPPHIRE(3, 1024, 7.5f, 10f, -2.9f, 10, ForgeTagFactory.ingredientSupplier("gems/sapphire"), false),
	AMETHYST(3, 1024, 7.5f, 9.5f, -2.9f, 20, ForgeTagFactory.ingredientSupplier("gems/amethyst"), false);

	private final int defaultHarvestLevel;
	private final int defaultDurability;
	private final int defaultEnchantability;
	private final float defaultEfficiency;
	private final float defaultDamage;
	private final float defaultAttackSpeed;

	private final Supplier<Ingredient> repairMaterial;
	private final boolean enabledByDefault;

	private BattleaxeMaterial(int harvestLevel, int durability, float efficiency, float damage, float axeSpeed,
			int enchantability, Supplier<Ingredient> repairMaterial, boolean enabledByDefault) {
		this.defaultHarvestLevel = harvestLevel;
		this.defaultDurability = durability;
		this.defaultEfficiency = efficiency;
		this.defaultDamage = damage;
		this.defaultEnchantability = enchantability;
		this.defaultAttackSpeed = axeSpeed;
		this.repairMaterial = repairMaterial;
		this.enabledByDefault = enabledByDefault;
	}

	public int getDefaultHarvestLevel() {
		return defaultHarvestLevel;
	}

	public int getDefaultDurability() {
		return defaultDurability;
	}

	public int getDefaultEnchantability() {
		return defaultEnchantability;
	}

	public float getDefaultEfficiency() {
		return defaultEfficiency;
	}

	public float getDefaultDamage() {
		return defaultDamage;
	}

	public float getDefaultAttackSpeed() {
		return defaultAttackSpeed;
	}

	public Ingredient getRepairMaterial() {
		return repairMaterial.get();
	}

	public boolean isEnabledByDefault() {
		return enabledByDefault;
	}
	
	/**
	 * 
	 * @return The registry key for the battleaxe with this material
	 */
	public String getRegistryKey() {
		return name().toLowerCase() + "_battleaxe";
	}
	
	@Override
	public String toString() {
		return name().toLowerCase();
	}

}
