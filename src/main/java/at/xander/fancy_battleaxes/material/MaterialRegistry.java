package at.xander.fancy_battleaxes.material;

import com.google.common.collect.ImmutableMap;

import at.xander.fancy_battleaxes.config.Configuration;
import at.xander.fancy_battleaxes.config.ToolStatConfig;
import net.minecraftforge.fml.event.config.ModConfigEvent;

public class MaterialRegistry {
	private final ImmutableMap<BattleaxeMaterial, BattleaxeTier> materialMap;

	public MaterialRegistry(Configuration config) {
		ImmutableMap.Builder<BattleaxeMaterial, BattleaxeTier> builder = new ImmutableMap.Builder<>();
		for (BattleaxeMaterial mat : BattleaxeMaterial.values()) {
			ToolStatConfig toolCfg = config.toolStats.getConfigForMaterial(mat);
			BattleaxeTier tier = new BattleaxeTier(toolCfg, mat);
			builder.put(mat, tier);
		}
		materialMap = builder.build();
	}

	public ImmutableMap<BattleaxeMaterial, BattleaxeTier> getAllMaterials() {
		return materialMap;
	}

	/**
	 * This is called when the config is loaded
	 * 
	 * @param e
	 */
	public void onConfigLoaded(ModConfigEvent e) {
		for (BattleaxeTier tier : materialMap.values()) {
			tier.setCfgLoaded();
		}
	}
}
