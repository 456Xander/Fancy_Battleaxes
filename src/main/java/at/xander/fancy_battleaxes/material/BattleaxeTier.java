package at.xander.fancy_battleaxes.material;

import at.xander.fancy_battleaxes.config.ToolStatConfig;
import net.minecraft.world.item.crafting.Ingredient;

public class BattleaxeTier implements IBattleaxeTier {

	private boolean cfgLoaded = false;
	private final ToolStatConfig config;
	private final BattleaxeMaterial material;

	public BattleaxeTier(ToolStatConfig config, BattleaxeMaterial material) {
		this.config = config;
		this.material = material;
	}

	public BattleaxeMaterial getMaterial() {
		return material;
	}

	@Override
	public int getUses() {
		return cfgLoaded ? config.getDurability() : material.getDefaultDurability();
	}

	@Override
	public float getSpeed() {
		return cfgLoaded ? config.getEfficiency() : material.getDefaultEfficiency();
	}

	@Override
	public float getAttackDamageBonus() {
		return 0;
	}

	@Override
	public int getLevel() {
		return cfgLoaded ? config.getHarvestLevel() : material.getDefaultHarvestLevel();
	}

	@Override
	public int getEnchantmentValue() {
		return cfgLoaded ? config.getEnchantability() : material.getDefaultEnchantability();
	}

	@Override
	public Ingredient getRepairIngredient() {
		return material.getRepairMaterial();
	}

	@Override
	public float getAttackDamage() {
		return cfgLoaded ? config.getAttackDamage() : material.getDefaultDamage();
	}

	@Override
	public float getAttackSpeed() {
		return cfgLoaded ? config.getAttackSpeed() : material.getDefaultAttackSpeed();
	}
	
	protected void setCfgLoaded() {
		cfgLoaded = true;
	}

}
