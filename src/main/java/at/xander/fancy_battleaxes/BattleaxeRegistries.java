package at.xander.fancy_battleaxes;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.mojang.serialization.Codec;

import at.xander.fancy_battleaxes.config.Configuration;
import at.xander.fancy_battleaxes.crafting.MaterialEnabledCondition;
import at.xander.fancy_battleaxes.material.BattleaxeMaterial;
import at.xander.fancy_battleaxes.material.BattleaxeTier;
import at.xander.fancy_battleaxes.material.MaterialRegistry;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BattleaxeRegistries {

	private final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, BattleaxeMod.MODID);
	private final DeferredRegister<Codec<? extends ICondition>> CONDITION_SERIALIZERS = DeferredRegister
			.create(ForgeRegistries.CONDITION_SERIALIZERS, BattleaxeMod.MODID);

	public final Map<BattleaxeMaterial, RegistryObject<Item>> battleaxes;
	private final Configuration config;
	private Set<BattleaxeMaterial> blacklist = EnumSet.noneOf(BattleaxeMaterial.class);

	public final RegistryObject<Codec<MaterialEnabledCondition>> material_enabled = CONDITION_SERIALIZERS
			.register("material_enabled", () -> MaterialEnabledCondition.CODEC);

	public BattleaxeRegistries(IEventBus evBus, Configuration config, MaterialRegistry matReg) {
		ITEMS.register(evBus);
		CONDITION_SERIALIZERS.register(evBus);
		this.config = config;
		evBus.addListener(this::onConfigLoaded);
		evBus.addListener(this::handleCreativeTabRegistration);
		ImmutableMap.Builder<BattleaxeMaterial, RegistryObject<Item>> builder = ImmutableMap.builder();
		for (var entry : matReg.getAllMaterials().entrySet()) {
			builder.put(entry.getKey(), registerBattleaxeForMat(entry.getKey(), entry.getValue()));
		}
		battleaxes = builder.build();
	}

	private RegistryObject<Item> registerBattleaxeForMat(BattleaxeMaterial material, BattleaxeTier tier) {
		var regID = material.getRegistryKey();
		Supplier<Item> itemSup = () -> new ItemBattleaxe(tier, new Item.Properties());
		return ITEMS.register(regID, itemSup);
	}

	private void onConfigLoaded(ModConfigEvent e) {
		for (var k : battleaxes.keySet()) {
			if (!config.toolEnables.get(k)) {
				blacklist.add(k);
			}
		}
	}

	private void handleCreativeTabRegistration(BuildCreativeModeTabContentsEvent event) {
		if (event.getTabKey() == CreativeModeTabs.COMBAT) {
			battleaxes.entrySet().stream().filter(e -> !blacklist.contains(e.getKey()))
					.forEach(e -> event.accept(e.getValue().get()));
		}
	}

}
