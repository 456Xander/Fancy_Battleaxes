package at.xander.fancy_battleaxes.config;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

import at.xander.fancy_battleaxes.material.BattleaxeMaterial;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;

public class ToolEnables {

	private Map<BattleaxeMaterial, BooleanValue> allowMap;

	public ToolEnables(ForgeConfigSpec.Builder cfgBuilder) {
		cfgBuilder.push("Enabled Tools");
		ImmutableMap.Builder<BattleaxeMaterial, BooleanValue> mapBuilder = ImmutableMap.builder();
		BattleaxeMaterial[] allMaterials = BattleaxeMaterial.values();
		for (BattleaxeMaterial value : allMaterials) {
			BooleanValue val = cfgBuilder.define(value.name().toLowerCase(), value.isEnabledByDefault());
			mapBuilder.put(value, val);
		}
		allowMap = mapBuilder.build();
		cfgBuilder.pop();
	}

	public boolean get(BattleaxeMaterial mat) {
		BooleanValue bVal = allowMap.get(mat);
		return bVal != null ? bVal.get() : false;
	}
}
