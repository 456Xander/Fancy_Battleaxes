package at.xander.fancy_battleaxes.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.event.config.ModConfigEvent;

public class Configuration {
	private ForgeConfigSpec config;

	private ForgeConfigSpec.Builder builder;

	public final ToolEnables toolEnables;
	public final ToolStats toolStats;

	public Configuration() {
		builder = new ForgeConfigSpec.Builder();
		toolEnables = new ToolEnables(builder);
		toolStats = new ToolStats(builder);
	}
	
	public ForgeConfigSpec getConfig() {
		if (config == null) {
			config = builder.build();
			builder = null;
		}
		return config;
	}
	
	/**
	 * NOTE: This function is registered on the EventBus by the main mod
	 */
	public void onConfigLoaded(ModConfigEvent e) {
		
	}
}
