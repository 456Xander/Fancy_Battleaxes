package at.xander.fancy_battleaxes.config;

import at.xander.fancy_battleaxes.material.BattleaxeMaterial;
import net.minecraftforge.common.ForgeConfigSpec.Builder;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

public class ToolStatConfig {

	/**
	 * All Float Config values are declared as Number here, because there were
	 * Problems, that the config reader produced a Double number, which could not be
	 * cast to Float.
	 */

	private final ConfigValue<Double> efficiency;
	private final ConfigValue<Integer> durability;
	private final ConfigValue<Integer> harvestLevel;
	private final ConfigValue<Integer> enchantability;
	private final ConfigValue<Double> attackDamage;
	private final ConfigValue<Double> attackSpeed;

	public ToolStatConfig(Builder builder, BattleaxeMaterial material) {
		builder.push(material.toString().toLowerCase());
		durability = builder.define("durability", material.getDefaultDurability());
		harvestLevel = builder.define("harvest_level", material.getDefaultHarvestLevel());
		efficiency = builder.define("efficiency", (double) material.getDefaultEfficiency());
		attackDamage = builder.define("attack_damage", (double) material.getDefaultDamage());
		attackSpeed = builder.define("attack_speed", (double) material.getDefaultAttackSpeed());
		enchantability = builder.define("enchantability", material.getDefaultEnchantability());
		builder.pop();
	}

	public float getEfficiency() {
		return efficiency.get().floatValue();
	}

	public int getDurability() {
		return durability.get().intValue();
	}

	public int getHarvestLevel() {
		return harvestLevel.get().intValue();
	}

	public int getEnchantability() {
		return enchantability.get().intValue();
	}

	public float getAttackDamage() {
		return attackDamage.get().floatValue();
	}

	public float getAttackSpeed() {
		return attackSpeed.get().floatValue();
	}

}
