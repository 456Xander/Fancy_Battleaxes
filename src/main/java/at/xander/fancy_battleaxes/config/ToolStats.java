package at.xander.fancy_battleaxes.config;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import at.xander.fancy_battleaxes.material.BattleaxeMaterial;
import net.minecraftforge.common.ForgeConfigSpec.Builder;

public class ToolStats {

	private final Map<BattleaxeMaterial, ToolStatConfig> configurations;

	public ToolStats(Builder builder) {
		Map<BattleaxeMaterial, ToolStatConfig> tempMap = new HashMap<>();
		builder.push("ToolStats");
		for (BattleaxeMaterial s : BattleaxeMaterial.values()) {
			tempMap.put(s, new ToolStatConfig(builder, s));
		}
		builder.pop();
		configurations = ImmutableMap.copyOf(tempMap);
	}
	
	public ToolStatConfig getConfigForMaterial(BattleaxeMaterial material) {
		return configurations.get(material);
	}

}