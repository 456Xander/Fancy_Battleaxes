package at.xander.fancy_battleaxes;

import java.util.logging.Logger;

import at.xander.fancy_battleaxes.config.Configuration;
import at.xander.fancy_battleaxes.material.MaterialRegistry;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(BattleaxeMod.MODID)
public class BattleaxeMod {

	public static final String MODID = "fancy_battleaxes";

	public Logger logger;

	public final Configuration config;

	public final BattleaxeRegistries itemHandler;
	private final MaterialRegistry matRegistry;
	
	public static BattleaxeMod instance;

	public BattleaxeMod() {
		instance = this;
		logger = Logger.getLogger(MODID);
		IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
		config = new Configuration();
		matRegistry = new MaterialRegistry(config);
		bus.addListener(matRegistry::onConfigLoaded);
		itemHandler = new BattleaxeRegistries(bus, config, matRegistry);
		ModLoadingContext.get().registerConfig(Type.COMMON, config.getConfig());
	}

}
